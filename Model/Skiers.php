<?php

class Skier{
	public $userName;
	public $firstName;
	public $lastName;
	public $yearOfBirth;
	//public $currentClub;
	
	/**
	Contructor for the class skiers
	@userName- unique username for the skier
	@firstName - skier's first name
	@lastName - skier's last name
	@yearOfBirth - skier's year  of birth
	@currentClub - skier's current club
	*/
	public function __construct($userName,$firstName,$lastName,$yearOfBirth){
		$this->userName = $userName;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->yearOfBirth = $yearOfBirth;	
		//$this->currentClub = $currentClub;
	}
}
?>