<?php

include_once 'Club.php';
include_once 'Entry.php';
include_once 'Season.php';
include_once 'Skiers.php';

class Model {
	protected $doc;
	protected $xpath;
	
	public function __construct(){
		$this->doc = new DOMDocument();
		$this->doc->load('SkierLogs.xml');
		$this->xpath = new DOMXpath($this->doc);
	}
	
	public function importSkiersFromXML(){
		$skiersArray = array();
		$skiers = $this->xpath->query('/SkierLogs/Skiers/Skier');
		
		foreach ($skiers as $Skier){
			$userName = $Skier->getAttribute('userName');
			$firstName = $Skier->getElementsByTagName('FirstName')->item(0)->textContent;
			$lastName = $Skier->getElementsByTagName('LastName')->item(0)->textContent;
			$yearOfBirth = $Skier->getElementsByTagName('YearOfBirth')->item(0)->textContent;
			$skiersArray[] = new Skier($userName,$firstName,$lastName,$yearOfBirth);
		}
		return $skiersArray;
}
	public function importClubsFromXML(){
		$clubsArray = array();
		$clubs = $this->xpath->query('/SkierLogs/Clubs/Club');
		
		foreach ($clubs as $Club){
			$clubID = $Club->getAttribute('id');
			$name = $Club->getElementsByTagName('Name')->item(0)->textContent;
			$city= $Club->getElementsByTagName('City')->item(0)->textContent;
			$county = $Club->getElementsByTagName('County')->item(0)->textContent;
			$clubsArray[] = new Club($clubID,$name,$city,$county);
		}
		return $clubsArray;
	}
	public function importSeasonsFromXML(){
	$seasonsArray = array();
	$seasons = $this->xpath->query('/SkierLogs/Season');
	
		foreach($seasons as $Season){
		$fallYear = $Season->getAttribute('fallYear');
		$clubs = $Season->getElementsByTagName('Skiers');
			foreach($clubs as $Club){
				$clubID = $Club->getAttribute('clubId');
				$skiers = $Club->getElementsByTagName('Skier');
					foreach($skiers as $Skier){
					$userName = $Skier->getAttribute('userName');
					$entries = $Skier->getElementsByTagName('Entry');
					$totalDistance = 0;
						foreach($entries as $Entry){
						$distance = $Entry->getElementsByTagName('Distance')->item(0)->nodeValue;
						$totalDistance+=$distance;
						}
				$seasonsArray[] = new Season($fallYear,$clubID,$userName,$totalDistance);
				}
			}
			
		}
		return $seasonsArray;
	}
	public function importEntriesFromXML(){
		$entriesArray = array();
		$skiers = $this->xpath->query('/SkierLogs/Season/Skiers/Skier');
		
		foreach ($skiers as $Skier){
			$userName = $Skier->getAttribute('userName');
			$entries = $Skier->getElementsByTagName('Entry');
			foreach($entries as $Entry){
				$date = $Entry->getElementsByTagName('Date')->item(0)->textContent;
				$area = $Entry->getElementsByTagName('Area')->item(0)->textContent;
				$distance = $Entry->getElementsByTagName('Distance')->item(0)->nodeValue;
				$entriesArray[]= new Entry($userName,$date,$area,$distance);
			}
		}
		return $entriesArray;
	}
}