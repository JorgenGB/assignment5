<?php

class Club{
	public $clubID;
	public $name;
	public $city;
	public $county;
	
	/**
	Contructor for the class club
	@clubID - unique id for the club
	@name - club name
	@city - city for the club
	@country - county for the club
	*/
	public function __construct($clubID,$name,$city,$county){
		$this->clubID = $clubID;
		$this->name = $name;
		$this->city = $city;
		$this->county = $county;	
	}
}
?>