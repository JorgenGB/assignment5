<?php

class Entry{
	public $serName;
	public $date;
	public $area;
	public $distance;
	
	/**
	Contructor for the class entry
	@userName - skier's userName
	@date - date of entry
	@area - area of entry
	@distance - distance of entry
	*/
	public function __construct($userName,$date,$area,$distance){
		$this->userName = $userName;
		$this->date = $date;
		$this->area= $area;
		$this->distance = $distance;	
	}
}
?>