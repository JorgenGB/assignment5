<?php
include_once("DBCredentials.php");
class DBModel{
	protected $db = null;
	protected $skiersArray;
	protected $clubArray;
	protected $seasonArray;
	protected $entriesArray;
	
	function __construct($db=null,$skiers,$clubs,$seasons,$entries){
		$this->skiersArray = $skiers;
		$this->clubArray = $clubs;
		$this->seasonArray = $seasons;
		$this->entriesArray = $entries;
		
		if($db){
			$this->db = $db;
		}else {
			$this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8',
                  DB_USER, DB_PWD, 
                  array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
	}
	public function addSkierIntoDB(){
		$res = $this->db->prepare('INSERT INTO skier(userName,firstName,lastName,yearOfBirth) VALUES(:userName,:firstName,:lastName,:yearOfBirth)');
		foreach ($this->skiersArray as $Skier){
			$res->bindParam(':userName',$Skier->userName,PDO::PARAM_STR);
			$res->bindParam(':firstName',$Skier->firstName,PDO::PARAM_STR);
			$res->bindParam(':lastName',$Skier->lastName,PDO::PARAM_STR);
			$res->bindParam(':yearOfBirth',$Skier->yearOfBirth,PDO::PARAM_STR);
			$res->execute();
		}
	}
	public function addClubIntoDB(){
		$res = $this->db->prepare('INSERT INTO club(clubID,name,city,county) VALUES(:clubID,:name,:city,:county)');
		foreach ($this->clubArray as $club){
			$res->bindParam(':clubID',$club->clubID,PDO::PARAM_STR);
			$res->bindParam(':name',$club->name,PDO::PARAM_STR);
			$res->bindParam(':city',$club->city,PDO::PARAM_STR);
			$res->bindParam(':county',$club->county,PDO::PARAM_STR);
			$res->execute();
		}
	}
	public function addSeasonIntoDB(){
		$res = $this->db->prepare('INSERT INTO season(fallYear,clubID,userName,totalDistance) VALUES(:fallYear,:clubID,:userName,:totalDistance)');
		foreach ($this->seasonArray as $season){
			$res->bindParam(':fallYear',$season->fallYear,PDO::PARAM_STR);
			$res->bindParam(':clubID',$season->clubID,PDO::PARAM_STR);
			$res->bindParam(':userName',$season->userName,PDO::PARAM_STR);
			$res->bindParam(':totalDistance',$season->totalDistance,PDO::PARAM_STR);
			$res->execute();
		}
	}
	public function addEntryIntoDB(){
		$res = $this->db->prepare('INSERT INTO entry(userName,date,area,distance) VALUES(:userName,:date,:area,:distance)');
		foreach ($this->entriesArray as $entry){
			$res->bindParam(':userName',$entry->userName,PDO::PARAM_STR);
			$res->bindParam(':date',$entry->date,PDO::PARAM_STR);
			$res->bindParam(':area',$entry->area,PDO::PARAM_STR);
			$res->bindParam(':distance',$entry->distance,PDO::PARAM_STR);
			$res->execute();
		}
	}
	
}


?>