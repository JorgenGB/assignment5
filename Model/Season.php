<?php

class Season{
	public $fallYear;
	public $clubID;
	public $userName;
	public $totalDistance;
	
	/**
	Contructor for the class season
	@fallYear - year of the season
	@clubID - id of the club
	@userName - skier's userName
	@totalDistance - total distance of the skier this season (for one club)
	*/
	public function __construct($fallYear,$clubID,$userName,$totalDistance){
		$this->fallYear = $fallYear;
		$this->clubID = $clubID;
		$this->userName = $userName;
		$this->totalDistance = $totalDistance;
	}
}
?>