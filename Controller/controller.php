<?php
include_once 'Model/Model.php';
include_once 'Model/DBModel.php';

class Controller {
	public $model;

	public function __construct(){
		session_start();
		$this->model = new Model();
	}
	public function invoke(){
		$skiers = $this->model->importSkiersFromXML();
		$clubs = $this->model->importClubsFromXML();
		$seasons = $this->model->importSeasonsFromXML();
		$entries = $this->model->importEntriesFromXML();
		$this->model =new DBModel($db=null,$skiers,$clubs,$seasons,$entries);
		$this->model->addSkierIntoDB();
		$this->model->addClubIntoDB();
		$this->model->addSeasonIntoDB();
		$this->model->addEntryIntoDB();
	}
}
?>
